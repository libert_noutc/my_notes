import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timezone/timezone.dart';
import 'package:timezone/data/latest.dart';


class LocalNotificationService {
  LocalNotificationService();

  final _localNotificationService = FlutterLocalNotificationsPlugin();
  
  
  Future<void> initialize() async {
    const AndroidInitializationSettings androidInitializationSettings =
    AndroidInitializationSettings('@drawable/ic_stat_mynote');
    DarwinInitializationSettings initializationSettingsIOS =
    DarwinInitializationSettings(
        requestSoundPermission: true,
        requestBadgePermission: true,
        requestAlertPermission: true,
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    final InitializationSettings  settings =
    InitializationSettings(android: androidInitializationSettings, iOS: initializationSettingsIOS);
    
    await _localNotificationService.initialize(settings,
        onDidReceiveNotificationResponse: onTapNotification);
  }
  Future<NotificationDetails> _notificationDetails() async {
    const AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
        'your channel id',
        'your channel name',
        channelDescription: 'your channel description',
        importance: Importance.max,
        priority: Priority.high,
        playSound: true,
    );

    const DarwinNotificationDetails  ioSNotificationDetails =  DarwinNotificationDetails();

    return NotificationDetails(
        iOS:ioSNotificationDetails,
        android: androidNotificationDetails );
  }

  Future<void> showNotification({required int id, required String title, required String body})
  async {
    final notificationDetails = await _notificationDetails();
    await _localNotificationService.show(id, title, body, notificationDetails);
  }

  void onTapNotification (NotificationResponse payload){
      print('$payload');
  }
  void onDidReceiveLocalNotification(int id, String? tile, String? body, String? Payload){
    print('id : $id');

  }

  void requestIOSPermissions() {
    _localNotificationService
        .resolvePlatformSpecificImplementation<
        IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

}