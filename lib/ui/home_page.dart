import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:my_notes/service/notification.dart';

import '../service/local_notification.dart';
import '../service/themes_service.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  late final LocalNotificationService localnotifyhelper  ;
  @override
  void initState() {
    localnotifyhelper = LocalNotificationService();
    localnotifyhelper.initialize();
    localnotifyhelper.requestIOSPermissions();
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: _appBar(

      ),
      body: Column(
        children: [
          Text("MyNote",
          style: TextStyle(
            fontSize:30,
          ))
        ],

      ),
    );
  }

  _appBar(){
    return AppBar(
      leading: GestureDetector(
        onTap: (){
          localnotifyhelper.showNotification(
            id:0,
            title: "Theme change notif",
            body:Get.isDarkMode?"dark mode on":"know is is light",
          );
          ThemesService().switchTheme();
        },
        child: Icon(Icons.nightlight_round,
          size: 20,),
      ),
        actions:[
          Icon(Icons.person, size: 20,
          ),
          SizedBox(width: 20),
        ]

    );
  }
}


